package com.davidminaya.bugaddroundrect

import android.content.Context
import android.graphics.Path
import android.graphics.Paint
import android.graphics.RectF
import android.graphics.Color
import android.graphics.Canvas
import android.view.View

/**
 * Creada por david minaya el 06/09/2018 11:13.
 *
 */
public class AddRoundRectView(context: Context?) : View(context) {

    // max = 8188
    val right = 8188f

    // pathRoundRect
    val pathRoundRect = Path()
    val paintRoundRect = Paint()

    // pathRect
    val pathRect = Path()
    val paintPathRect = Paint()

    // rect
    val rect = RectF()
    val paintRect = Paint()

    init {

        // pathRoundRect
        var esquinas = floatArrayOf(150f, 150f, 150f, 150f, 150f, 150f, 150f, 150f)
        pathRoundRect.addRoundRect(0f, 500f, right, 800f, esquinas, Path.Direction.CCW)
        paintRoundRect.isAntiAlias = true
        paintRoundRect.color = Color.BLUE

        // pathRect
        pathRect.addRect(0f, 900f, right, 1200f, Path.Direction.CCW)
        paintPathRect.isAntiAlias = true
        paintPathRect.color = Color.GREEN

        // rect
        rect.set(0f, 1300f, right, 1600f)
        paintRect.isAntiAlias  = true
        paintRect.color = Color.RED
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        // Esto es para poder llamar las funciones de canvas sin utilizar el signo de interrogacion
        if (canvas == null) return

        canvas.drawColor(Color.BLACK)

        // Reduce la escala del lienzo para que los rectangulos se puedan ver completos en la pantalla
        canvas.scale(0.04f, 0.04f)

        // pathRoundRect
        canvas.drawPath(pathRoundRect, paintRoundRect)

        // pathRect
        canvas.drawPath(pathRect, paintPathRect)

        // rect
        canvas.drawRect(rect, paintRect)

    }

}